const express = require("express");
const bodyParser = require("body-parser");
const corsMiddleware = require("cors");

const FCM = require("fcm-node");
const serverKey =
  "AAAAK7pWjrk:APA91bGPYdNzf59LmBWa5iCDcvNd07EA_Pn0egImQLp2wI_Ilidnj5aMnePjvTjtoYU4yOLy80rLvYgvU4A4J0PNtuLxxr3FO7d3dN4QE-NhKzKYAR445Yr9pF115ydEUWvU7-xQMPb1";
const fcm = new FCM(serverKey);

const app = express();

const routes = require("./routes");

app.use(
  corsMiddleware({
    domain: "http://10.10.2.96:8080/"
  })
);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/", routes);

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Origin",
    "Origin, X-Requested-With, Content-Type, Accept," + "Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

setInterval(function() {
  const message = {
    to: global.deviceId,
    data: {
      title: global.titleNotification,
      body: global.descriptNotification,
      identifierNumber: global.todoId
    }
  };

  if (global.timeofNotification == Date.now() && global.done === false) {
    fcm.send(message, function(err, response) {
      message.title = global.titleNotification;
    });
  }
}, 1);

module.exports = app;
